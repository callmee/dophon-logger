from dophon_properties import *
get_property(DOPHON)
from dophon_logger import *

logger = get_logger(HUES)
logger.inject_logger(globals())

# print(logger)

logger.info('info')
logger.warning('warning')
logger.error('error')
# logger.log('log')
logger.debug('debug')