# dophon-logger

#### 项目介绍
dophon logger module
dophon框架日志功能模块,依赖于dophon properties模块

#### 软件架构
本模块仅仅为根据配置以及用户传入参数进行日志模块选择,并进行缓存
通过模块中的inject_logger方法对当前python文件的全局变量进行运行时赋值
尽量使模块对象的创建更贴近使用时刻,防止内存的空闲占用(即在生命周期中的创建到使用的时间)


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)